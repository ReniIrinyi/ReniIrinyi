### Hi there 👋  i´m Reni,

- 🔭 I’m currently learning at SGD as [Software-Developer (Zertifizierte/r Agile/r Software-Developer/in)](https://www.sgd.de/kursseite/agiler-software-developer.html?referrer=S_MAS_SK_000001&gclid=CjwKCAiAxvGfBhB-EiwAMPakqmCOUISyxYvQv70TRG9I85lqvqk25CQMjfF7TaGu1T6HKRSxMZe0ZRoCQrIQAvD_BwE) (Java, SpringBoot, JavaScript, Vue, React, Node.js). 
- 😄 I really enjoy problem solving as it gives me great satisfaction when I find a solution to a complex problem.
- 👯 I am very enthusiastic about coding and I want to work with
people who are equally passionate about it!
- 💬 You can contact me in German, English or Hungarian

